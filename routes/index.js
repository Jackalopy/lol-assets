module.exports = function(app, assets, data) {
  app.route('/data/:type')
    .get((req, res) => {
      let type = req.params.type

      switch(type) {
        case 'champions':
          res.status(200).send({
            data: data.champions,
            md5: data.md5.champions
          })
          break
        case 'items':
          res.status(200).send({
            data: data.items,
            md5: data.md5.items
          })
          break
        case 'spells':
          res.status(200).send({
            data: data.spells,
            md5: data.md5.spells
          })
          break
        case 'perks':
          res.status(200).send({
            data: data.perks,
            md5: data.md5.perks
          })
          break
        case 'version':
          assets.getVersion().then(version => res.send(version))
          .catch(console.error)
          break
        case 'all':
          assets.getVersion().then((version) =>
          {
            res.send({
              version,
              champions: {
                data: data.champions,
                md5: data.md5.champions
              },
              items: {
                data: data.items,
                md5: data.md5.items
              },
              spells: {
                data: data.spells,
                md5: data.md5.spells
              },
              perks: {
                data: data.perks,
                md5: data.md5.perks
              }
            })
          })
          .catch(console.error)

      }

    })

  app.route('/version')
    .get((req, res) => {
      assets.getVersion().then((version) =>
      {
        console.log(version)
        res.send(version)
      })
      .catch(console.error)
    })

  app.route('/:specie/:type/:queue')
    .get((req, res) => {
      let type = req.params.type,
          specie = req.params.specie
          queue = req.params.queue,
          skin = req.query.skin? req.query.skin : 0

      let champion, item, spell, perk

      switch(specie) {
        case 'champion-icon':
          switch(type) {
            case 'by-id':
              champion = data.champions.filter(champion => champion.key == queue)[0]
              res.status(200).sendFile(assets.getChampion('icon', champion.id))
              break

            case 'by-name':
              champion = data.champions.filter(champion => champion.id.toLowerCase() == queue.toLowerCase())[0]
              res.status(200).sendFile(assets.getChampion('icon', champion.id))
              break

            default:
              res.status(400).send('Error')
              break
          }

          break

        case 'champion-loading':
          switch(type) {
            case 'by-id':
              champion = data.champions.filter(champion => champion.key == queue)[0]
              if(champion) {
                if(champion.id.toLowerCase() === 'fiddlesticks') champion.id = 'FiddleSticks'
                res.status(200).sendFile(assets.getChampion('loading', champion.id, skin))
              }
              break

            case 'by-name':
              champion = data.champions.filter(champion => champion.id.toLowerCase() == queue.toLowerCase())[0]
              if(champion) {
                if(champion.id.toLowerCase() === 'fiddlesticks') champion.id = 'FiddleSticks'
                res.status(200).sendFile(assets.getChampion('loading', champion.id, skin))
              }
              break

            default:
              res.status(400).send('Error')
              break
          }
          break

        case 'champion-splash':
          switch(type) {
            case 'by-id':
              champion = data.champions.filter(champion => champion.key == queue)[0]
              if(champion) {
                if(champion.id.toLowerCase() === 'fiddlesticks') champion.id = 'FiddleSticks'
                res.status(200).sendFile(assets.getChampion('splash', champion.id, skin))
              } else res.status(404).send()
              break

            case 'by-name':
              champion = data.champions.filter(champion => champion.id.toLowerCase() == queue.toLowerCase())[0]
              if(champion) {
                if(champion.id.toLowerCase() === 'fiddlesticks') champion.id = 'FiddleSticks'
                res.status(200).sendFile(assets.getChampion('splash', champion.id, skin))
              } else res.status(404).send()
              break

            default:
              res.status(400).send('Error')
              break
          }
          break
        case 'champion-tile':
          switch(type) {
            case 'by-id':
              champion = data.champions.filter(champion => champion.key == queue)[0]
              if(champion) {
                if(champion.id.toLowerCase() === 'fiddlesticks') champion.id = 'FiddleSticks'
                res.status(200).sendFile(assets.getChampion('tile', champion.id, skin))
              } else res.status(404).send()
              break

            case 'by-name':
              champion = data.champions.filter(champion => champion.id.toLowerCase() == queue.toLowerCase())[0]
              if(champion) {
                if(champion.id.toLowerCase() === 'fiddlesticks') champion.id = 'FiddleSticks'
                res.status(200).sendFile(assets.getChampion('tile', champion.id, skin))
              } else res.status(404).send()
              break

            default:
              res.status(400).send('Error')
              break
          }
          break

        case 'profile-icon':
          switch(type) {
            case 'by-id':
              res.status(200).sendFile(assets.getProfileIcon(queue))
              break

            default:
              res.status(400).send('Error')
              break
          }
          break

        case 'item':
          switch(type) {
            case 'by-id':
              item = data.items.filter(item => item.key == queue)[0]
              if(item) res.status(200).sendFile(assets.getItem(item.key))
              else res.status(400).send('Error')
              break

            default:
              res.status(400).send('Error')
              break
          }
          break

        case 'spell':
          switch(type) {
            case 'by-id':
              spell = data.spells.filter(spell => spell.key == queue)[0]
              if(spell) res.status(200).sendFile(assets.getSpell(spell.id))
              else res.status(400).send('Error')
              break

            case 'by-name':
              spell = data.spells.filter(spell => spell.id == queue)[0]
              if(spell) res.status(200).sendFile(assets.getSpell(spell.id))
              else res.status(400).send('Error')
              break

            default:
              res.status(400).send('Error')
              break
          }
          break

        case 'perk':
          switch(type) {
            case 'by-id':
              perk = data.perks.filter(perk => perk.id == queue)[0]
              if(perk) res.status(200).sendFile(assets.getPerk(perk.icon))
              else res.status(400).send('Error')
              break

            case 'by-name':
              perk = data.perks.filter(perk => perk.key == queue)[0]
              if(perk) res.status(200).sendFile(assets.getPerk(perk.icon))
              else res.status(400).send('Error')
              break

            default:
              res.status(400).send('Error')
              break
          }
          break

        default:
          res.status(404).send('Error')
          break
      }
    })
}
