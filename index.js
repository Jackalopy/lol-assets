const express = require('express'),
  bodyParser = require('body-parser'),
  path = require('path'),
  assert = require('assert'),
  cors = require('cors'),
  app = express(),
  schedule = require('node-schedule'),
  port = process.env.PORT || 3001,
  routes = require('./routes'),
  Assets = require('./controllers/Assets'),
  Data = require('./controllers/Data')

let assets = new Assets(),
    data
    
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors({ origin: function (origin, callback) {
  let whitelist = ['https://ganqs.com', 'https://api.ganqs.com']
  var corsOptions;
  if (whitelist.indexOf(origin) !== -1)
  corsOptions = { origin: true }
  else
  corsOptions = { origin: false }
  callback(null, corsOptions)
}}))

assets.setup()
.then(() => {
  let update = schedule.scheduleJob('0 0 0,3,6,9,12,15,18,21 * * *', () => {
    assets.setup()
      .then(data.update)
  })
  data = new Data(assets)
  routes(app, assets, data)
  app.listen(port)

  console.log(`Server Assets listening at port ${port}.`)
})
