const request = require('request'),
      rp = require('request-promise'),
      path = require('path'),
      fs = require('fs'),
      tar = require('tar'),
      readline = require('readline'),
      fsp = fs.promises

const rmFullDir = delPath => {
  return fsp.lstat(delPath)
    .then(() => fsp.readdir(delPath))
    .then(files => {
      return Promise.all(files.map(file => {
        let curPath = path.join(delPath, file)
        if (fs.lstatSync(curPath).isDirectory()) {
          return rmFullDir(curPath)
        } else {
          console.log('Removing file: ', curPath)
          return fsp.unlink(curPath)
        }
      }))

    })
    .then(() => {
      console.log('Removing Folder: ', delPath)
      return fsp.rmdir(delPath)
    })
}

module.exports = class Assets {
  constructor() {
    this.ddragon = 'https://ddragon.leagueoflegends.com/',
    this.cdn = this.ddragon + 'cdn/'
    this.homeDir = path.join(__dirname, '../')
    this.updating = false
    this.assetsDir = path.join(this.homeDir, 'assets')
    this.unfinishedDir = path.join(this.assetsDir, 'unfinished')
    this.readyDir = path.join(this.assetsDir, 'ready')
    this.oldDir = path.join(this.assetsDir, 'old')
    this.downloadDir = path.join(this.homeDir, 'download')

    this.setup = this.setup.bind(this)
    this.update = this.update.bind(this)
    this.getRiotVersion = this.getRiotVersion.bind(this)
    this.getVersion = this.getVersion.bind(this)
    this.getData = this.getData.bind(this)
    this.getChampion = this.getChampion.bind(this)
    this.getProfileIcon = this.getProfileIcon.bind(this)
    this.getItem = this.getItem.bind(this)
    this.getSpell = this.getSpell.bind(this)
    this.getPerk = this.getPerk.bind(this)
  }

  setup(fakeRiotVersion) {
    console.log('starting setup')
    let assetsExists = fsp.stat(this.assetsDir).catch(err => {
        if(err.errno === -2)
          return fsp.mkdir(this.assetsDir).then(Promise.all([fsp.mkdir(this.readyDir), fsp.mkdir(this.unfinishedDir), fsp.mkdir(this.unfinishedDir)]))
        else
          return err
      }),
      downloadExists = fsp.stat(this.downloadDir).catch(err => {
        if(err.errno === -2) return fsp.mkdir(this.downloadDir).then(created)
        else return err
      })

    return Promise.all([assetsExists, downloadExists]).then(values => {
      let files = []

      files.push(fsp.stat(this.readyDir).catch(err => {
        if(err.errno === -2) return fsp.mkdir(this.readyDir)
        else return err
      }))

      return Promise.all(files).then(() => {

        return Promise.all([this.getVersion(), this.getRiotVersion(fakeRiotVersion)])
          .then(values => {
            const version = values[0], riotVersion = values[1]
            if(version === riotVersion) {
              this.version = version
              console.log(`Everything is up to date. Server is using version ${riotVersion}. Using version ${this.version}`)
              return this.version
            } else {
              return this.update(riotVersion)
            }
          })
      })
    })
  }

  update(newVersion) {
    if(this.updating) return
    this.updating = true;

    let uri = `${this.ddragon}cdn/dragontail-${newVersion}.tgz`,
        file = path.join(this.downloadDir, newVersion + '.tgz')
    console.log(`\nUpdating from ${this.version} to ${newVersion} ------------------------------------------------------\n`)

    fsp.stat(this.oldDir)
      .then(() => {
        console.log('Removing Old Folder\n')
        rmFullDir(this.oldDir)
      })
      .catch(err => {
        if(err.errno === -2) return
        else return err
      })

    fsp.stat(this.unfinishedDir).catch(err => {
      if(err.errno === -2) return fsp.mkdir(this.unfinishedDir)
      else return err
    })

    return fsp.readdir(this.downloadDir)
      .then(files => files.map(file => path.join(this.downloadDir, file)))
      .then(() => {
        let req = request(uri),
            length = 0, downloaded = 0

        req.pipe(fs.createWriteStream(file))

        return new Promise((res, rej) => {
          req
          .on('response', data => {
            length = Number(data.headers['content-length'])
            console.log(`\nStarting download of ${(length/1000000).toFixed(2)} MB`)
            process.stdout.write('Download: 0%')
          })
          .on('data', data => {
            downloaded += data.length
            readline.clearLine(process.stdout)
            readline.cursorTo(process.stdout, 0)
            process.stdout.write('Download: ' + (downloaded/length*100).toFixed(2) + '%')
          })
          .on('close', data => {
            console.log('\nDownload Completed.\n')
            console.log('Extracting file...')
            tar.x({ cwd: this.unfinishedDir, file, newer: true })
            .then(data => {
              console.log('Finished extracting file.\n')
              console.log('Last preparations...')
              fsp.rename(this.readyDir, this.oldDir)
              .then(() => fsp.rename(this.unfinishedDir, this.readyDir))
              .then(() => {
                return Promise.all([
                  fsp.copyFile(path.join(this.readyDir, newVersion, 'manifest.json'), path.join(this.readyDir, 'manifest.json')),
                  rmFullDir(this.oldDir)
                ])
              })
              .then(() => console.log('\nIt\'s done. --------------------------------------------------------------\n'))
              .then(() => this.getVersion(true))
              .then(() => res(this.version))
            })
          })
        })
      })
  }

  getRiotVersion(fakeRiotVersion) {
    if(fakeRiotVersion) return fakeRiotVersion

    let url = this.ddragon + 'realms/na.json'
    return rp(url).then(raw => JSON.parse(raw).dd)
  }

  getVersion(forced = false) {
    if(this.version && !forced) {
      return new Promise((res, rej) => res(this.version))
    }
    else {
      let manifestFile = path.join(this.readyDir, `manifest.json`)
      return fsp.stat(manifestFile)
        .then(stat => fsp.readFile(manifestFile))
        .then(raw => {
          this.version = JSON.parse(raw).dd
          return this.version
        })
        .catch(err => false)
    }
  }

  getData(type, queue) {
    switch(type) {
      case 'items':
        return JSON.parse(fs.readFileSync(path.join(this.readyDir, this.version, 'data/en_US', 'item.json')))
      case 'champions':
        return JSON.parse(fs.readFileSync(path.join(this.readyDir, this.version, 'data/en_US', 'champion.json')))
      case 'spells':
        return JSON.parse(fs.readFileSync(path.join(this.readyDir, this.version, 'data/en_US', 'summoner.json')))
      case 'perks':
        return JSON.parse(fs.readFileSync(path.join(this.readyDir, this.version, 'data/en_US', 'runesReforged.json')))
    }
  }

  getChampion(type, champion, skin) {
    switch(type) {
      case 'icon':
        return path.join(this.readyDir, this.version, 'img', 'champion', `${champion}.png`)
        break
      case 'loading':
        return path.join(this.readyDir, 'img', 'champion', 'loading', `${champion}_${skin}.jpg`)
        break
      case 'splash':
        return path.join(this.readyDir, 'img', 'champion', 'splash', `${champion}_${skin}.jpg`)
        break
      case 'tile':
        return path.join(this.readyDir, 'img', 'champion', 'tiles', `${champion}_${skin}.jpg`)
        break
    }
  }

  getProfileIcon(id) {
    return path.join(this.readyDir, this.version, 'img', 'profileicon', `${id}.png`)
  }

  getItem(id) {
    return path.join(this.readyDir, this.version, 'img', 'item', `${id}.png`)
  }

  getSpell(name) {
    return path.join(this.readyDir, this.version, 'img', 'spell', `${name}.png`)
  }

  getPerk(icon) {
    return path.join(this.readyDir, 'img', icon)
  }
}
