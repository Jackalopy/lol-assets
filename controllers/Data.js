const rp = require('request-promise'),
      md5 = require('md5')

module.exports = class Data {
  constructor(assets) {
    this.assets = assets
    this.assets.getVersion().then(version => {
      this.version = version
      this.champions = []
      this.spells = []
      this.perks = []
      this.items = []
      this.md5 = {
        champions: null,
        spells: null,
        perks: null,
        items: null
      }
      this.update()
    })
  }

  update() {
    Object.entries(this.assets.getData('champions').data).forEach(champion => {
      champion = champion[1]
      if(champion._id) delete champion._id
      this.champions.push(champion)
    })

    Object.entries(this.assets.getData('spells').data).forEach(spell => {
      spell = spell[1]
      if(spell._id) delete spell._id
      this.spells.push(spell)
    })

    Object.entries(this.assets.getData('items').data).forEach(item => {
      item[1].key = item[0]
      item = item[1]
      if(item._id) delete item._id
      this.items.push(item)
    })

    Object.entries(this.assets.getData('perks')).forEach(perk => {
      perk = perk[1]
      if(perk._id) delete perk._id
      this.perks.push(perk)
    })

    this.perks.forEach(tree => {
      tree._id = tree.id
      this.perks.push(tree)
      tree.slots.forEach(slot => slot.runes.forEach(rune => {
        rune._id = rune.id
        this.perks.push(rune)
      }))
    })

    Object.entries(this.md5).forEach(values => {
      this.md5[values[0]] = md5(JSON.stringify(this[values[0]]))
    })
  }
}
